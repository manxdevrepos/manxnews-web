import { TabAppsComponent } from './components/app-body/tab-apps/tab-apps.component';
import { TabLanguageComponent } from './components/app-body/tab-language/tab-language.component';
import { Routes, RouterModule } from '@angular/router'
import { TabPlatformComponent } from "./components/app-body/tab-platform/tab-platform.component";

const APP_ROUTES: Routes = [
    //-- Que por defecto te mande a la ventana de reciente siempre.
    { path: '', redirectTo: '/apps', pathMatch: 'full' },
    { path: 'apps', component: TabAppsComponent },
    { path: 'languages', component: TabLanguageComponent },
    { path: 'platforms', component: TabPlatformComponent },
];

export const TabRoutes = RouterModule.forRoot(APP_ROUTES);