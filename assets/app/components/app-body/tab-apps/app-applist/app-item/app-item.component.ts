import { Component, Input } from '@angular/core';
import { AppService } from "../../app.service";
import { App } from "../../app.model";

@Component({
  selector: 'app-app-item',
  templateUrl: './app-item.component.html',
  styleUrls: ['./app-item.component.css']
})
export class AppItemComponent {

  //-- Variables ---------------------------------------------------------------------------------/
  @Input()
  inApp: App;

  //-- Constructor -------------------------------------------------------------------------------/
  constructor(
    private oAppService: AppService
  ) { }

  //-- Metodos -----------------------------------------------------------------------------------/

  /**
   * Se usa cuando se selecciona el item de la lista. Emite el evento de app seleccionada.
   */
  onSelect() {
    this.oAppService.selectApp(this.inApp);
  }
}