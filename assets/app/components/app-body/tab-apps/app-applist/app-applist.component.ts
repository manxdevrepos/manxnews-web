import { Component, OnInit, Input } from '@angular/core';
import { AppService } from "../app.service";
import { App } from "../app.model";
import { AppInputComponent } from "../app-input/app-input.component";

@Component({
	selector: 'app-applist',
	templateUrl: 'app-applist.component.html',
	styleUrls: [ './app-applist.component.css' ]
})

export class AppApplistComponent implements OnInit {

	//-- Variables ---------------------------------------------------------------------------------/
	_appList: App[];
	_appSelected: App = null;

	@Input()
	inAppModal: AppInputComponent;

	//-- Constructor -------------------------------------------------------------------------------/
	constructor(
		private oAppService: AppService
	) { }

	//-- Overrided methods -------------------------------------------------------------------------/
	ngOnInit() {

		//-- Llenando la lista de apps.
		this.oAppService.getApps().subscribe(
			(oAppList: App[]) => {

				//TODO: Lista de apps no se actualiza.
				this._appList = oAppList;
			}
		);

		//-- App seleccionada.
		this.oAppService.appIsSelected.subscribe(
			(oApp: App) => {

				//-- Actualizamos nuestro objeto.
				this._appSelected = oApp;
			}
		);
	}
}