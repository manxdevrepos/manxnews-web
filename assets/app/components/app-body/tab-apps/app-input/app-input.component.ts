import { Validators } from '@angular/forms';
import { FormControl } from '@angular/forms';
import { FormGroup } from '@angular/forms';
import { Component, OnInit, ViewChild } from '@angular/core';
import { ModalComponent } from "ng2-bs3-modal";
import { App } from "../app.model";
import { Language } from "../../tab-language/language.model";
import { Platform } from "../../tab-platform/platform.model";
import { PlatformService } from "../../tab-platform/platform.service";
import { LanguageService } from "../../tab-language/language.service";
import { AppService } from "../app.service";

@Component({
  selector: 'app-app-input',
  templateUrl: './app-input.component.html',
  styleUrls: ['./app-input.component.css']
})
export class AppInputComponent implements OnInit {

  //-- Variables ---------------------------------------------------------------------------------/
  _stringFormTitle: String;
  _app: App;
  _languageList: Language[];
  _platformList: Platform[];
  _formApp: FormGroup;

  @ViewChild('modalApp')
  modal: ModalComponent;

  //-- Constructor -------------------------------------------------------------------------------/
  constructor(
    private oLanguageService: LanguageService,
    private oPlatformService: PlatformService,
    private oAppService: AppService
  ) { }

  //-- Overrided methods -------------------------------------------------------------------------/
  ngOnInit() {

    //--Validadores para la forma.
    this._formApp = new FormGroup({
      app_name: new FormControl(null, [
        Validators.required,
        Validators.maxLength(30)
      ]),
      app_lang: new FormControl(null, [
        Validators.required
      ]),
      app_plat: new FormControl(null, [
        Validators.required
      ])
    });

    //-- Llenando la lista de lenguajes.
    this.oLanguageService.getLanguages().subscribe(
      (oLanguagesList: Language[]) => {
        this._languageList = oLanguagesList;
      }
    );

    //-- Llenando la lista de plataformas.
    this.oPlatformService.getPlatforms().subscribe(
      (oPlatformList: Platform[]) => {
        this._platformList = oPlatformList;
      }
    );

    //-- Suscrito al evento de editar mensaje.
    this.oAppService.appIsEdit.subscribe(
      (oApp: App) => {

        //-- Cambiamos el titulo de la forma.
        this._stringFormTitle = "Editar aplicacion";

        //-- Actualizamos nuestro objeto.
        this._app = oApp;

        //-- Mostramos el modal.
        this.modal.open();
      }
    );
  }

  //-- Methods -----------------------------------------------------------------------------------/

  /**
  *  Muestra el objeto modal, usado para accesarlo desde otros componentes.
  */
  openModal() {
    this._stringFormTitle = "Agregar aplicacion"
    this._app = null;
    this.modal.open();
  }

  /**
   *  Llama al servicio para editar o agregar una app y lo subscribe para
   *  su resultado.
   */
  onSubmit() {

    //-- Cerrar el modal.
    this.modal.close();

    //-- Si el objeto existe, estamos editando.
    //console.log(this._app);
    if (this._app) {
      console.log("Editando app");

      this._app._title = this._formApp.value.app_name;
      this._app._languageId = this._formApp.value.app_lang;
      this._app._platformId = this._formApp.value.app_plat;
      //console.log('Enviando');
      //console.log(this._app);
      this.oAppService.updateApp(this._app)
        .subscribe(
        oResult => console.log(oResult),
        oError => console.error(oError)
        );

    } else {
      console.log('Nueva app');

      //-- Si no, es nuevo.
      const app = new App(
        this._formApp.value.app_name,
        this._formApp.value.app_lang,
        this._formApp.value.app_plat
      );

      this.oAppService.addApp(app)
        .subscribe(
        oResult => console.log(oResult),
        oError => console.error(oError)
        );
    }

    //-- Limpiando todo.
    this.onClear();
  }

  /**
   * Limpia los campos usados para agregar/editar una app.
   */
  onClear() {
    this._app = null;
    this._formApp.reset();
  }
}