import { Headers, Http, Response } from '@angular/http';
import { App } from './app.model';
import { Injectable, EventEmitter, Output } from '@angular/core';
import 'rxjs';
import { Observable } from 'rxjs';
import { Language } from "../tab-language/language.model";

@Injectable()
export class AppService {

    //-- Variables -----------------------------------------------------------------------------------/
    private _appList: App[] = [];
    appIsEdit = new EventEmitter<App>();
    appIsSelected = new EventEmitter<App>();
    languageOfAppSelected = new EventEmitter<{ app: App, lang: Language }>();

    //-- Constructor ---------------------------------------------------------------------------------/
    constructor(private oHttp: Http) { }

    //-- Methods -------------------------------------------------------------------------------------/

    /**
     * Emite un evento para desplegar las noticias de la app en la tabla en 1 lenguaje.
     * @param oApp          Objeto app seleccionado
     * @param oLanguage     Objeto language seleccionado.
     */
    selectLanguageOfApp(oApp: App, oLanguage: Language) {
        this.languageOfAppSelected.emit({
            app: oApp,
            lang: oLanguage
        });
    }

    /**
     * Obtiene 1 app en especifico en base a la id.
     * @param oAppId EL id del lenguaje a buscar.
     */
    getSingleApp(oAppId: String) {
        return this._appList.find(
            (oApp: App) => oApp._id === oAppId
        );
    }

    /**
     * Emite un evento para desplegar la info de la app en el panel
     * @param oApp     Objeto app que se va a visualizar.
     */
    selectApp(oApp: App) {
        this.appIsSelected.emit(oApp);
    }

    /**
     * Emite un evento para desplegar la info de la app en la forma.
     * @param oApp     Objeto app que se va a agregar.
     */
    editApp(oApp: App) {
        this.appIsEdit.emit(oApp);
    }

    /**
     * Elimins un lenguaje en el arreglo de lenguajes dentro de la app y en la base de datos.
     * @param oLanguage     Objeto lenguaje que se va a elimina.
     */
    deleteApp(oApp: App) {
        this._appList.splice(this._appList.indexOf(oApp), 1);
        const body = JSON.stringify(oApp);
        const headers = new Headers({ 'Content-Type': 'application/json' });
        return this.oHttp.patch('http://localhost:3000/db/deleteApp/' + oApp._id, body, { headers: headers })
            .map((response: Response) => {
                return response.json();
            })
            .catch((error: Response) => Observable.throw(error.json));
    }

    /**
     * Actualiza una app ya existente con nuevos datos.
     * @param oApp     La app con los datos nuevos y con su identificador.
     */
    updateApp(oApp: App) {
        const body = JSON.stringify(oApp);
        const headers = new Headers({ 'Content-Type': 'application/json' });
        return this.oHttp.patch('http://localhost:3000/db/updateApp/' + oApp._id, body, { headers: headers })
            .map((response: Response) => {
                return response.json();
            })
            .catch((error: Response) => Observable.throw(error.json));
    }

    /**
     * Agrega una app en el arreglo de apps y en la base de datos.
     * @param oApp     Objeto app que se va a agregar.
     */
    addApp(oApp: App) {
        const body = JSON.stringify(oApp);
        const headers = new Headers({ 'Content-Type': 'application/json' });
        return this.oHttp.post('http://localhost:3000/db/addApp', body, { headers: headers })
            .map((response: Response) => {

                const result = response.json();
                const app = new App(result.obj.title, result.obj.languageId, result.obj.platformId, result.obj._id);
                this._appList.push(app);
                return result;
            })
            .catch((error: Response) => Observable.throw(error.json));
    }

    /**
     * Obtiene todas las apps que se encuentran almacenados en la base de datos y los regresa 
     * en un arreglo.
     */
    getApps() {
        return this.oHttp.get('http://localhost:3000/db/getApps')
            .map((oResponse: Response) => {
                const oAppList = oResponse.json().obj;
                let transformedApp: App[] = [];
                for (let app of oAppList) {
                    transformedApp.push(new App(app.title, app.languageId, app.platformId, app._id));
                }
                this._appList = transformedApp;
                //console.log(this._appList);
                return transformedApp;
            })
            .catch((error: Response) => Observable.throw(error.json()));
    }
}