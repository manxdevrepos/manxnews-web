import { Component, OnInit, SimpleChanges, Input, ViewChild } from '@angular/core';
import { App } from "../app.model";
import { Language } from "../../tab-language/language.model";
import { AppService } from "../app.service";
import { LanguageService } from "../../tab-language/language.service";
import { PlatformService } from "../../tab-platform/platform.service";
import { AppInputComponent } from "../app-input/app-input.component";
import { NewsService } from "./news-table/news.service";
import { News } from "./news-table/news.model";
import { ModalComponent } from "ng2-bs3-modal";

@Component({
	selector: 'app-infoPanel',
	templateUrl: 'app-infoPanel.component.html',
	styleUrls: ['./app-infoPanel.component.css']

})

export class AppInfoPanelComponent implements OnInit {


	//-- Variables -------------------------------------------------------------------------------/
	@ViewChild("newsInput")
	_newsModal: ModalComponent;

	@ViewChild("newsDelete")
	_newsModalDelete: ModalComponent;

	@Input()
	inAppModal: AppInputComponent;

	inApp: App;
	_languageList: Language[] = [];

	//-- Constructor -----------------------------------------------------------------------------/
	constructor(
		private oAppService: AppService,
		private oLanguageService: LanguageService,
		private oPlatformService: PlatformService,
		private oNewsService: NewsService,
	) { }

	//-- Methods -----------------------------------------------------------------------------------/

	/**
	 * Se llama cuando se hace clic en alguno de los items de las noticias.
	 * @param oLanguage Lenguaje que fue seleccionado.
	 */
	onSelect(oLanguage: Language) {
		this.oAppService.selectLanguageOfApp(this.inApp, oLanguage);
	}

	/**
   	 * Llama al servicio de editar plataforma con la info que tiene.
     */
	onEdit() {
		this.oAppService.editApp(this.inApp);
	}

	/**
     * Muestra un dialogo para confirmar la eliminacion de la app, lo desactiva en la base de datos
     * y lo borra en el arreglo.
     */
	onDelete() {
		//TODO: FALTA que se cheque si el lenguaje esta en uso, de ser asi, no puede ser desactivado.
		//TODO: Esta funcion debe de implementar el nuevo modal.

		this.oAppService.deleteApp(this.inApp).subscribe(
			oResult => console.log(oResult),
			oError => console.error(oError)
		);
		this._newsModalDelete.close();
		this.inApp = null;
	}

	//-- Overrided methods -------------------------------------------------------------------------/

	ngOnInit() {

		//-- Para obtener todos los lenguajes que se usan, nos suscribimos cuando la app cambie.
		this.oAppService.appIsSelected.subscribe(
			(oApp: App) => {

				//-- Actualizamos nuestro objeto.
				this.inApp = oApp;

				//-- Limpiamos nuestra lista.
				this._languageList = [];

				//-- Obtener noticias.
				this.oNewsService.getNews().subscribe((oNewsList: News[]) => {

					//-- Por cada noticia.
					oNewsList.forEach((paramNew: News) => {

						//-- Si son de la app.
						if (paramNew._appId === this.inApp._id) {

							//-- Buscamos el lenguaje.
							let currentLanguage = this.oLanguageService.getSingleLanguage(paramNew._languageId);
							currentLanguage._abv

							//-- Si no lo tenemos ya en el arreglo.
							if (!this._languageList.includes(currentLanguage))
								this._languageList.push(currentLanguage);
						}
					});

					//-- Si despues de todo esto el arreglo no tiene noticias, es porque la app no tiene, se pone nada mas el lenguaje de la app.
					if (this._languageList.length < 1) {
						this._languageList.push(this.oLanguageService.getSingleLanguage(this.inApp._languageId));
					}
				});
			});
	}
}