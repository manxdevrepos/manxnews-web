import { Component, Input, OnInit } from '@angular/core';
import { Language } from "../../../tab-language/language.model";
import { App } from "../../app.model";
import { LanguageService } from "../../../tab-language/language.service";
import { AppService } from "../../app.service";
import { News } from "./news.model";
import { NewsService } from "./news.service";

@Component({
    selector: 'app-news-table',
    templateUrl: './news-table.component.html',
    styleUrls: ['./news-table.component.css']
})

export class NewsTableComponent implements OnInit {

    //-- Variables -------------------------------------------------------------------------------/
    private _app: App;
    private _language: Language;
    private _newsList: News[] = [];

    //-- Constructor -----------------------------------------------------------------------------/
    constructor(
        private oAppService: AppService,
        private oNewsList: NewsService
    ) { }

    //-- Overrided methods -----------------------------------------------------------------------/
    ngOnInit(): void {
        this.oAppService.languageOfAppSelected.subscribe(
            (data) => {
                this._app = data.app;
                this._language = data.lang;

                //-- Vaciamos la lista de noticias actual.
                this._newsList = [];

                //-- Obteniendo las noticias que nos interesan.
                this.oNewsList.getNews().subscribe(
                    (oNews: News[]) => {
                        oNews.forEach(
                            (oNew : News) => {

                                //-- Checar en cada noticia si se tiene la app y lenguaje que nos interesan.
                                if(oNew._appId === this._app._id && oNew._languageId === this._language._id) {
                                    this._newsList.push(oNew);
                                }
                        })
                    }
                );
            }
        );
    }
}