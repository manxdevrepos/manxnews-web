import { Component, ViewChild } from '@angular/core';
import { AppInputComponent } from "./app-input/app-input.component";

@Component({
  selector: 'app-tab-apps',
  templateUrl: './tab-apps.component.html',
  styleUrls: ['./tab-apps.component.css']
})
export class TabAppsComponent {

  //-- Variables ---------------------------------------------------------------------------------/
  @ViewChild("appInput")
  _appModal: AppInputComponent;

  //-- Constructor -------------------------------------------------------------------------------/
  constructor() { }
}