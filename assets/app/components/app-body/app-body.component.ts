import { LanguageService } from './tab-language/language.service';
import { Component } from '@angular/core';
import { AppService } from '../app-body/tab-apps/app.service'
import { PlatformService } from "./tab-platform/platform.service";
import { NewsService } from "./tab-apps/app-infoPanel/news-table/news.service";

@Component({
  selector: 'app-body',
  templateUrl: './app-body.component.html',
  styleUrls: ['./app-body.component.css'],
  providers: [
    LanguageService,
    PlatformService,
    AppService,
    NewsService
  ]
})

/**
 * Aqui se obtienen todos los servicios usados dentro de la app.
 */
export class AppBodyComponent {
  constructor() { }
}