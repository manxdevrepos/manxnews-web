import { PlatformService } from './../platform.service';
import { Platform } from './../platform.model';
import { Component, OnInit, ViewChild } from '@angular/core';
import { PlatformInputComponent } from "./platform-input/platform-input.component";

@Component({
  selector: 'platform-table',
  templateUrl: './platform-table.component.html',
  styleUrls: ['./platform-table.component.css']
})

//TODO: FALTA que el boton de eliminar cuente si se esta usando estA platafdorma en alguna app.
//TODO: BUSCADOR debe de funcionar correctamente.

export class PlatformTableComponent implements OnInit {

  //-- Variables ---------------------------------------------------------------------------------/
  _platformList: Platform[];

  @ViewChild("platformInput")
  _platformModal: PlatformInputComponent;

  //-- Constructor -------------------------------------------------------------------------------/
  constructor(
    private oPlatformService: PlatformService
  ) { }

  //-- Overrided methods -------------------------------------------------------------------------/
  ngOnInit() {
    this.oPlatformService.getPlatforms().subscribe(
      (oLanguagesList: Platform[]) => { 
        this._platformList = oLanguagesList;
      }
    );
  }
}