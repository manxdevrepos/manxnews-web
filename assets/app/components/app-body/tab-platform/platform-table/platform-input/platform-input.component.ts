import { Component, OnInit, ElementRef, ViewChild } from '@angular/core';
import { FormGroup, FormControl, Validators } from "@angular/forms";
import { ModalComponent } from "ng2-bs3-modal";
import { Platform } from "../../platform.model";
import { PlatformService } from "../../platform.service";

@Component({
  selector: 'platform-input',
  templateUrl: './platform-input.component.html',
  styleUrls: ['./platform-input.component.css']
})

/**
 * Se encarga de la creacion de plataformas. Maneja la info dentro de la forma, la valida y la envia 
 * por un servicio con onSubmit.
 */
export class PlatformInputComponent implements OnInit {

  //-- Variables ---------------------------------------------------------------------------------/
  _stringFormTitle: String;
  _platform: Platform;
  _formPlatform: FormGroup;

  @ViewChild('modalPlatform')
  modal: ModalComponent;

  //-- Constructor -------------------------------------------------------------------------------/
  constructor(private oPlatformService: PlatformService) { }

  //-- Overrided methods -------------------------------------------------------------------------/
  ngOnInit() {

    //--Validadores para la forma.
    this._formPlatform = new FormGroup({
      plat_name: new FormControl(null, [
        Validators.required,
        Validators.maxLength(30)
      ])
    });

    //-- Suscrito al evento de editar plataforma.
    this.oPlatformService.platformIsEdit.subscribe(
      (oPlatform: Platform) => {

        //-- Cambiamos el titulo de la forma.
        this._stringFormTitle = "Editar plataforma";

        //-- Actualizamos nuestro objeto.
        this._platform = oPlatform;

        //-- Mostramos el modal.
        this.modal.open();
      }
    );
  }

  //-- Methods -----------------------------------------------------------------------------------/

  /**
   *  Muestra el objeto modal, usado para accesarlo desde otros componentes.
   */
  openModal() {
    this._stringFormTitle = "Agregar plataforma"
    this._platform = null;
    this.modal.open();
  }

  /**
   *  Llama al servicio para editar o agregar una plataforma y lo subscribe para
   *  su resultado.
   */
  onSubmit() {

    //-- Cerrar el modal.
    this.modal.close();

    //-- Si el objeto existe, estamos editando.
    if (this._platform) {
      this._platform._title = this._formPlatform.value.plat_name;
      this.oPlatformService.updatePlatform(this._platform)
        .subscribe(
        oResult => console.log(oResult),
        oError => console.error(oError)
        );

    } else {

      //-- Si no, es nuevo.
      const platform = new Platform(
        this._formPlatform.value.plat_name,
      );
      this.oPlatformService.addPlatform(platform)
        .subscribe(
        oResult => console.log(oResult),
        oError => console.error(oError)
        );
    }

    //-- Limpiando todo.
    this.onClear();
  }

  /**
   * Limpia los campos usados para agregar/editar un lenguaje.
   */
  onClear() {
    this._platform = null;
    this._formPlatform.reset();
  }
}