import { LanguageService } from './../language.service';
import { Language } from './../language.model';
import { Component, OnInit, ViewChild } from '@angular/core';
import { ModalComponent } from "ng2-bs3-modal";
import { LanguageInputComponent } from "./language-input/language-input.component";

@Component({
  selector: 'language-table',
  templateUrl: './language-table.component.html',
  styleUrls: ['./language-table.component.css']
})

//TODO: FALTA que el boton de eliminar cuente si se esta usando este lenguaje en alguna app o noticia.
//TODO: BUSCADOR debe de funcionar correctamente.

export class LanguageTableComponent implements OnInit {

  //-- Variables ---------------------------------------------------------------------------------/
  _languageList: Language[];
  filter = { _title: '' };

  @ViewChild("languageInput")
  _languageModal: LanguageInputComponent;

  //-- Constructor -------------------------------------------------------------------------------/
  constructor(
    private oLanguageService: LanguageService
  ) { }

  //-- Overrided methods -------------------------------------------------------------------------/

  ngOnInit() {
    this.oLanguageService.getLanguages().subscribe(
      (oLanguagesList: Language[]) => {
        //console.log(oLanguagesList);
        this._languageList = oLanguagesList;
      }
    );
  }
}