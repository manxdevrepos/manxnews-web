import { LanguageItemComponent } from './components/app-body/tab-language/language-table/language-item/language-item.component';
import { ReactiveFormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { LanguageTableComponent } from './components/app-body/tab-language/language-table/language-table.component';
import { ModalModule } from 'angular2-modal';
import { BootstrapModalModule } from 'angular2-modal/plugins/bootstrap';
import { LanguageInputComponent } from './components/app-body/tab-language/language-table/language-input/language-input.component';
import { TabAppsComponent } from './components/app-body/tab-apps/tab-apps.component';
import { TabRoutes } from './app.routing';
import { AppBodyComponent } from './components/app-body/app-body.component';
import { AppHeaderComponent } from './components/app-header/app-header.component';
import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { AppComponent } from "./app.component";
import { Ng2Bs3ModalModule } from 'ng2-bs3-modal/ng2-bs3-modal';
import { TabLanguageComponent } from "./components/app-body/tab-language/tab-language.component";
import { PlatformInputComponent } from "./components/app-body/tab-platform/platform-table/platform-input/platform-input.component";
import { PlatformItemComponent } from "./components/app-body/tab-platform/platform-table/platform-item/platform-item.component";
import { PlatformTableComponent } from "./components/app-body/tab-platform/platform-table/platform-table.component";
import { TabPlatformComponent } from "./components/app-body/tab-platform/tab-platform.component";
import { AppInputComponent } from "./components/app-body/tab-apps/app-input/app-input.component";
import { AppInfoPanelComponent } from "./components/app-body/tab-apps/app-infoPanel/app-infoPanel.component";
import { NewsTableComponent } from "./components/app-body/tab-apps/app-infoPanel/news-table/news-table.component";
import { AppApplistComponent } from "./components/app-body/tab-apps/app-applist/app-applist.component";
import { NewsItemComponent } from "./components/app-body/tab-apps/app-infoPanel/news-table/news-item/news-item.component";
import { NewsInputComponent } from "./components/app-body/tab-apps/app-infoPanel/news-table/news-input/news-input.component";
import { AppItemComponent } from "./components/app-body/tab-apps/app-applist/app-item/app-item.component";


@NgModule({
    declarations: [
        AppComponent,
        AppHeaderComponent,
        AppBodyComponent,
        TabAppsComponent,
        TabPlatformComponent,
        TabLanguageComponent,
        LanguageInputComponent,
        LanguageTableComponent,
        LanguageItemComponent,
        PlatformInputComponent,
        PlatformItemComponent,
        PlatformTableComponent,
        AppInputComponent,
        AppApplistComponent,
        AppInfoPanelComponent,
        NewsTableComponent,
        AppItemComponent,
        NewsInputComponent,
        NewsItemComponent
    ],
    imports: [
        BrowserModule,
        TabRoutes,
        ModalModule.forRoot(),
        BootstrapModalModule,
        HttpModule,
        ReactiveFormsModule,
        Ng2Bs3ModalModule
    ],
    bootstrap: [AppComponent]
})
export class AppModule {

}